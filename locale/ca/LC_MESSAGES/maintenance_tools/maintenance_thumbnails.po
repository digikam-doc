# Translation of docs_digikam_org_maintenance_tools___maintenance_thumbnails.po to Catalan
# Copyright (C) 2023-2024 This_file_is_part_of_KDE
# Licensed under the <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">terms of the GNU Free Documentation License 1.2</a> unless stated otherwise
# This file is distributed under the same license as the digiKam Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2023.
# Josep M. Ferrer <txemaq@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: digikam-doc\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-12-16 02:29+0000\n"
"PO-Revision-Date: 2024-12-23 21:01+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: ../../maintenance_tools/maintenance_thumbnails.rst:1
msgid "digiKam Maintenance Tool to Rebuild Thumbnails"
msgstr "L'eina de manteniment del digiKam Reconstrueix les miniatures"

#: ../../maintenance_tools/maintenance_thumbnails.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, maintenance, thumbnails"
msgstr ""
"digiKam, documentació, manual d'usuari, gestió de fotografies, codi obert, "
"lliure, aprenentatge, fàcil, manteniment, miniatures"

#: ../../maintenance_tools/maintenance_thumbnails.rst:14
msgid ":ref:`Rebuild Thumbnails <maintenance_tools>`"
msgstr ":ref:`Reconstruir les miniatures <maintenance_tools>`"

#: ../../maintenance_tools/maintenance_thumbnails.rst:20
msgid "The digiKam Maintenance Options to Rebuild Thumbnails"
msgstr ""
"Les opcions de manteniment del digiKam per a «Reconstrueix les miniatures»"

#: ../../maintenance_tools/maintenance_thumbnails.rst:22
msgid ""
"digiKam automatically generates and maintains thumbnails, but the **Rebuild "
"Thumbnail** tool might be required if you have worked on your images with "
"other applications or if you have changed the thumbnail size."
msgstr ""
"El digiKam genera i manté automàticament les miniatures, però l'eina "
"**Reconstrueix les miniatures** podria ser necessària si heu treballat en "
"les vostres imatges amb altres aplicacions o si heu canviat la mida de les "
"miniatures."

#: ../../maintenance_tools/maintenance_thumbnails.rst:24
msgid ""
"From :menuselection:`Settings --> Configure digiKam... --> View page --> "
"Tree-Views tab`, the option **Use Large Thumbnail Size for High Screen "
"Resolution** can be turned on to render **Icon-View** with a large thumbnail "
"size suitable for use on a 4K monitor. By default this option is turned off "
"and the maximum thumbnail size is limited to 256x256 pixels."
msgstr ""
"Des del plafó :menuselection:`Arranjament --> Configura el digiKam... --> "
"pàgina Vistes --> pestanya Vistes en arbre` podreu activar l'opció **Usa una "
"mida gran de les miniatures per a les pantalles d'alta resolució** per a "
"renderitzar la **vista d'icones** amb una mida de miniatura gran adequada "
"per a l'ús en un monitor 4K. De manera predeterminada, aquesta opció estarà "
"desactivada i la mida màxima de les miniatures estarà limitada a 256x256 "
"píxels."

#: ../../maintenance_tools/maintenance_thumbnails.rst:26
msgid ""
"When this option is enabled, thumbnail size can be extended to 512x512 "
"pixels. This option will increase the size of the thumbnail database and "
"will use more system memory. digiKam needs to be restarted for this option "
"to take effect, and this **Rebuild Thumbnails** tool needs to be run over "
"all collections."
msgstr ""
"Quan aquesta opció està activada, la mida de les miniatures es podrà ampliar "
"fins a 512x512 píxels. Aquesta opció augmentarà la mida de la base de dades "
"de miniatures i utilitzarà més memòria del sistema. El digiKam s'haurà de "
"reiniciar perquè faci efecte, i s'haurà de processar l'opció **Reconstrueix "
"les miniatures** de l'eina de manteniment sobre totes les col·leccions."

#: ../../maintenance_tools/maintenance_thumbnails.rst:32
msgid "The digiKam Views Setup Panel and the Large Thumbnail Option"
msgstr ""
"El diàleg de configuració Vistes amb l'opció per a miniatura grans en el "
"digiKam"

#: ../../maintenance_tools/maintenance_thumbnails.rst:36
msgid ""
"digiKam stores thumbnails in the database using the wavelet-compressed PGF "
"format, but large collections can still generate huge thumbnail databases. "
"Take care to use a sufficiently large storage media with a large amount of "
"available space to store the database."
msgstr ""
"El digiKam emmagatzema les miniatures a la base de dades utilitzant el "
"format PGF comprimit per ondetes, però les col·leccions grans encara poden "
"generar bases de dades enormes de miniatures. Aneu amb compte d'utilitzar un "
"mitjà d'emmagatzematge prou gran amb una gran quantitat d'espai disponible "
"per a emmagatzemar la base de dades."

#: ../../maintenance_tools/maintenance_thumbnails.rst:40
msgid ""
"The **Scan** options from :menuselection:`Settings --> Configure digiKam... "
"--> Miscelleneous page --> Behaviour tab` speed up the process in the first "
"case or if you just added pictures to your collection."
msgstr ""
"Les opcions **Explora** d':menuselection:`Arranjament --> Configura el "
"digiKam... --> pàgina Miscel·lània --> pestanya Comportament` acceleren el "
"procés en el primer cas o si acabeu d'afegir imatges a la col·lecció."

#: ../../maintenance_tools/maintenance_thumbnails.rst:42
msgid ""
"While the rebuild thumbnails process is underway, a progress indicator is "
"displayed in the bottom right corner of the main window."
msgstr ""
"Mentre el procés de reconstrucció de miniatures està en marxa, es mostra un "
"indicador de progrés a la cantonada inferior dreta de la finestra principal."

#: ../../maintenance_tools/maintenance_thumbnails.rst:48
msgid "The Rebuild Thumbnails Process Working in the Background"
msgstr "El procés de reconstrucció de les miniatures treballa en segon pla"

#: ../../maintenance_tools/maintenance_thumbnails.rst:52
msgid ""
"This process can also be directly called by the keyboard shortcut :kbd:`F5` "
"to rebuild the thumbnails for the currently selected album."
msgstr ""
"Aquest procés també es pot invocar directament mitjançant la drecera de "
"teclat :kbd:`F5` per a reconstruir les miniatures per a l'àlbum actualment "
"seleccionat."
