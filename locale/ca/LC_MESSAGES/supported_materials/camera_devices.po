# Translation of docs_digikam_org_supported_materials___camera_devices.po to Catalan
# Copyright (C) 2023-2024 This_file_is_part_of_KDE
# Licensed under the <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">terms of the GNU Free Documentation License 1.2</a> unless stated otherwise
# This file is distributed under the same license as the digiKam Manual package.
#
# SPDX-FileCopyrightText: 2023, 2024 Josep M. Ferrer <txemaq@gmail.com>
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: digikam-doc\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-12-08 01:42+0000\n"
"PO-Revision-Date: 2024-12-06 16:32+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: ../../supported_materials/camera_devices.rst:1
msgid "Camera and Mass Storage Devices Supported by digiKam"
msgstr "Càmeres i dispositius d'emmagatzematge massiu admesos pel digiKam"

#: ../../supported_materials/camera_devices.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, camera, gPhoto, usb, mass, storage"
msgstr ""
"digiKam, documentació, manual d'usuari, gestió de fotografies, codi obert, "
"lliure, aprenentatge, fàcil, càmera, gPhoto, usb, massiu, emmagatzematge"

#: ../../supported_materials/camera_devices.rst:14
msgid ":ref:`Cameras and Mass Storage Devices <supported_materials>`"
msgstr ""
":ref:`Càmeres i dispositius d'emmagatzematge massiu <supported_materials>`"

#: ../../supported_materials/camera_devices.rst:16
msgid "Contents"
msgstr "Contingut"

#: ../../supported_materials/camera_devices.rst:19
msgid "Introduction"
msgstr "Introducció"

# skip-rule: t-acc_obe
#: ../../supported_materials/camera_devices.rst:21
msgid ""
"An easy-to-use camera interface is provided that will download photographs "
"directly from your digital camera into digiKam Albums. More than `2700 "
"digital cameras <http://www.gphoto.org/proj/libgphoto2/support.php>`_ are "
"supported by the gPhoto library. Of course, any media or card reader "
"supported by your operating system will also interface with digiKam."
msgstr ""
"Es proporciona una interfície de càmera fàcil d'usar que descarregarà "
"fotografies directament des de la càmera digital als àlbums del digiKam. La "
"biblioteca gPhoto admet més de `2.700 càmeres digitals <http://www.gphoto."
"org/proj/libgphoto2/support.php>`_. Per descomptat, qualsevol suport o "
"lector de targetes compatible amb el sistema operatiu es cGairebé totes les "
"càmeres digitals recents suporten la versió 1 d'USB i, eventualment, la "
"versió 2 d'USB.onnectarà amb el digiKam."

#: ../../supported_materials/camera_devices.rst:25
msgid ""
"Camera import is currently not supported on Windows Systems due limitations "
"in the Gphoto2 library."
msgstr ""
"Actualment, la importació de càmera no està implementada als sistemes "
"Windows per limitacions en la biblioteca Gphoto2."

#: ../../supported_materials/camera_devices.rst:27
msgid ""
"digiKam fully supports RAW files, using the libraw library for reading and "
"processing RAW image files. You can find out if your particular camera is "
"supported by accessing the :menuselection:`Help --> Supported RAW Cameras` "
"menu."
msgstr ""
"El digiKam admet completament els fitxers RAW, utilitzant la biblioteca "
"«libraw» per a llegir i processar fitxers d'imatges RAW. Podeu esbrinar si "
"la càmera en particular és compatible accedint al menú :menuselection:`Ajuda "
"--> Càmeres RAW admeses`."

#: ../../supported_materials/camera_devices.rst:29
msgid ""
"See the :ref:`RAW Decoding Settings <setup_raw>` section for information on "
"setting up digiKam to work with RAW files. And see the :ref:`RAW Workflow "
"<rawprocessing_workflow>` section for a description of how to work with RAW "
"files in digiKam."
msgstr ""
"Vegeu la secció :ref:`configuració de la descodificació RAW <setup_raw>` per "
"a obtenir informació sobre com configurar el digiKam per a treballar amb "
"fitxers RAW. I vegeu la secció :ref:`Flux de treball RAW "
"<rawprocessing_workflow>` per a una descripció de com treballar amb fitxers "
"RAW al digiKam."

#: ../../supported_materials/camera_devices.rst:31
msgid ""
"Most current digital cameras store data on Compact Flash™ Memory cards and "
"use USB or FireWire (IEEE-1394 or i-link) connections for data transmission. "
"The actual transfers to a host computer are commonly carried out using the "
"USB Mass Storage device class (so that the camera appears as a disk drive) "
"or using the Picture Transfer Protocol (PTP) and its derivatives. "
"Alternatively, older cameras may use a Serial Port (RS-232) connection."
msgstr ""
"La majoria de les càmeres digitals actuals emmagatzemen les dades en "
"targetes Compact Flash™ Memory i fan servir connexions USB o FireWire "
"(IEEE-1394 o i-Link) per a la transmissió de les dades. Les transferències "
"reals a un ordinador habitualment es duen a terme utilitzant una classe de "
"dispositiu d'emmagatzematge massiu USB (de manera que la càmera apareixerà "
"com una unitat de disc) o utilitzant el protocol per a la transferència "
"d'imatges (Picture Transfer Protocol, PTP) i els seus derivats. Les càmeres "
"antigues poden utilitzar una connexió de port en sèrie (RS-232)."

#: ../../supported_materials/camera_devices.rst:34
msgid "Transfers using gPhoto: PTP and Serial Port"
msgstr "Transferències mitjançant el gPhoto: PTP i port en sèrie"

#: ../../supported_materials/camera_devices.rst:36
msgid ""
"digiKam employs the gPhoto library to communicate with digital still "
"cameras. gPhoto is a free, redistributable set of digital camera software "
"applications that supports a growing number of cameras. gPhoto supports the "
"Picture Transfer Protocol, a widely supported protocol developed by the "
"International Imaging Industry Association to allow the transfer of images "
"from digital cameras to computers and other peripheral devices without the "
"need of additional device drivers."
msgstr ""
"El digiKam utilitza la biblioteca gPhoto per a comunicar-se amb les càmeres "
"fotogràfiques digitals. El gPhoto és un conjunt lliure i redistribuïble "
"d'aplicacions de programari per a càmeres digitals que admet un nombre "
"creixent de càmeres. El gPhoto és compatible amb el protocol de "
"transferència d'imatges, un protocol àmpliament admès desenvolupat per "
"l'Associació Internacional de la Indústria d'Imatges per a permetre la "
"transferència d'imatges des de càmeres digitals cap a ordinadors i altres "
"dispositius perifèrics sense necessitat de controladors de dispositiu "
"addicionals."

# skip-rule: t-acc_obe
#: ../../supported_materials/camera_devices.rst:38
msgid ""
"Many older digital still cameras used the Serial Port to communicate with "
"host computers. Because photographs are big files and serial port transfers "
"are slow, this connection is now obsolete. digiKam still supports these "
"cameras and performs image transfers using the gPhoto program. You can find "
"a complete list of supported digital cameras at `this url <http://www.gphoto."
"org/proj/libgphoto2/support.php>`_."
msgstr ""
"Moltes càmeres fotogràfiques digitals més antigues feien servir el port en "
"sèrie per a comunicar-se amb els ordinadors amfitrions. Com que les "
"fotografies són fitxers grans i les transferències del port en sèrie són "
"lentes, aquesta connexió ara és obsoleta. El digiKam encara admet aquestes "
"càmeres i fa transferències d'imatges mitjançant el programa gPhoto. "
"Trobareu una llista completa de càmeres digitals admeses en `aquest URL "
"<http://www.gphoto.org/proj/libgphoto2/support.php>`_."

#: ../../supported_materials/camera_devices.rst:42
msgid ""
"gPhoto needs to be built with libexif to properly retrieve thumbnails for "
"use in digiKam. Exif support is required for thumbnail retrieval on some "
"libgphoto2 camera drivers. If Exif support is not set with gPhoto, you might "
"not see thumbnails or the thumbnail extraction may be very slow."
msgstr ""
"El gPhoto s'ha de construir amb la «libexif» per a rebre adequadament les "
"miniatures per a usar-les al digiKam. Cal compatibilitat amb Exif per a la "
"recuperació de miniatures en alguns controladors de càmera de la libgphoto2. "
"Si la compatibilitat amb Exif no està establerta amb el gPhoto, és possible "
"que no veieu les miniatures o que l'extracció d'aquestes sigui molt lenta."

#: ../../supported_materials/camera_devices.rst:48
msgid "The digiKam Setup Dialog to Configure a gPhoto Camera Device"
msgstr ""
"El diàleg de configuració del digiKam per a configurar un dispositiu de "
"càmera de gPhoto"

#: ../../supported_materials/camera_devices.rst:51
msgid "Transfers using Mass Storage device"
msgstr "Transferències emprant un dispositiu d'emmagatzematge massiu"

#: ../../supported_materials/camera_devices.rst:53
msgid ""
"For devices that are not directly supported by gPhoto, there is support for "
"the Mass Storage protocol, which is well supported under GNU/Linux®. This "
"includes many digital cameras and Memory Card Readers. Mass Storage "
"interfaces are:"
msgstr ""
"Per a dispositius que no són admesos directament pel gPhoto, hi ha suport "
"per al protocol d'emmagatzematge massiu, compatible amb GNU/Linux®. Això "
"inclou moltes càmeres digitals i lectors de targetes de memòria. Les "
"interfícies d'emmagatzematge massiu són:"

#: ../../supported_materials/camera_devices.rst:55
msgid ""
"**USB Mass Storage**: a computer interface using communication protocols "
"defined by the USB Implementers Forum that runs on the Universal Serial Bus. "
"This standard provides an interface to a variety of storage devices, "
"including digital cameras. Almost all recent digital cameras support USB "
"version 1 and eventually will support USB version 2."
msgstr ""
"**Emmagatzematge massiu USB**: una interfície d'ordinador que utilitza "
"protocols de comunicació definits pel Fòrum d'implementadors d'USB que "
"s'executen sobre el bus sèrie universal. Aquest estàndard proporciona una "
"interfície per a una varietat de dispositius d'emmagatzematge, incloses les "
"càmeres digitals. Gairebé totes les càmeres digitals recents admeten la "
"versió 1 d'USB i, a la llarga, la versió 2 d'USB."

#: ../../supported_materials/camera_devices.rst:57
msgid ""
"**FireWire Mass Storage**: a computer interface using communication "
"protocols developed primarily by Apple Computer in the 1990s. FireWire "
"offers high-speed communications and isochronous real-time data services. "
"Like USB Mass Storage, this standard provides an interface to a variety of "
"storage devices, including digital still cameras, although few cameras "
"support FireWire."
msgstr ""
"**Emmagatzematge massiu FireWire**: una interfície d'ordinador que utilitza "
"protocols de comunicació desenvolupats principalment per Apple Computer a la "
"dècada de 1990. El FireWire ofereix comunicacions d'alta velocitat i serveis "
"de dades isòcrones en temps real. Igual que l'emmagatzematge massiu USB, "
"aquest estàndard proporciona una interfície per a una varietat de "
"dispositius d'emmagatzematge, incloses les càmeres fotogràfiques digitals, "
"encara que molt poques admeten el FireWire."

#: ../../supported_materials/camera_devices.rst:59
msgid ""
"To use a generic Mass Storage device with digiKam, select :menuselection:"
"`Import --> Camera --> Add Camera Manually...`, add your device and set the "
"correct mount point path."
msgstr ""
"Per a utilitzar un dispositiu d'emmagatzematge massiu genèric amb el "
"digiKam, seleccioneu l'element de menú :menuselection:`Importa --> Càmera --"
"> Afegeix una càmera manualment...`, afegiu el dispositiu i establiu el camí "
"del punt de muntatge correcte."

#: ../../supported_materials/camera_devices.rst:61
msgid ""
"For details see the :ref:`Camera settings <camera_settings>` section of the "
"Setup Application chapter."
msgstr ""
"Per als detalls, vegeu la secció :ref:`Configuració de la càmera "
"<camera_settings>` del capítol sobre la configuració de l'aplicació."
