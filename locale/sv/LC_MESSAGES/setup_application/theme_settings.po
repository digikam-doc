# Copyright (C) licensed under the  <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">licensed under the terms of the GNU Free Documentation License 1.2+</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
#
# SPDX-FileCopyrightText: 2024, 2025 Stefan Asserhäll <stefan.asserhall@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.4.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-12-15 02:27+0000\n"
"PO-Revision-Date: 2025-02-08 16:50+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@gmail.com>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 24.12.0\n"

#: ../../setup_application/theme_settings.rst:1
msgid "digiKam Theme Settings"
msgstr "Temainställningar i digiKam"

#: ../../setup_application/theme_settings.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, colors, theme, configuration"
msgstr ""
"digiKam, dokumentation, användarhandbok, fotohantering, öppen källkod, fri, "
"lära, enkel, färger, tema, inställning"

#: ../../setup_application/theme_settings.rst:14
msgid ":ref:`Theme Settings <setup_application>`"
msgstr ":ref:`Temainställningar <setup_application>`"

#: ../../setup_application/theme_settings.rst:16
msgid ""
"Color themes are supplied allowing you to personalize the digiKam main "
"interface. To access these settings select :menuselection:`Settings --> "
"Themes` from the menubar and select your preferred theme."
msgstr ""
"Färgteman levereras så att man kan anpassa digiKams huvudgränssnitt. För att "
"komma åt inställningarna, välj :menuselection:`Inställningar --> Teman` från "
"menyraden och välj det tema som ska användas."

#: ../../setup_application/theme_settings.rst:22
msgid "The digiKam Color Themes Menu"
msgstr "Menyn Färgteman i digiKam"

#: ../../setup_application/theme_settings.rst:24
msgid ""
"Selecting a theme changes all of the elements of the graphical interface "
"consistent with the theme's color scheme."
msgstr ""
"Att välja ett tema ändrar alla element i det grafiska gränssnittet i "
"enlighet med temats färgschema."

#: ../../setup_application/theme_settings.rst:28
msgid ""
"Depending on the color theme you selected, you may have to restart digiKam "
"to load all of the relevant icons associated with the new color scheme."
msgstr ""
"Beroende på vilket färgtema som används, kanske man måste starta om digiKam "
"för att ladda alla relevanta ikoner som hör ihop med det nya färgschemat."

#: ../../setup_application/theme_settings.rst:30
msgid ""
"Some color schemes do not work well with some widget styles. So you may find "
"it necessary to adjust the **Widget Style** from the :menuselection:"
"`Settings --> Configure digiKam... --> Miscellaneous page --> Appearance "
"tab`, especially with dark color schemes. See :ref:`this section "
"<appearance_settings>` of this manual for more details about the "
"**Appearance** options."
msgstr ""
"Vissa färgscheman fungerar inte bra med vissa komponentstilar. Så det kan "
"vara nödvändigt att justera **Komponentstil** från fliken :menuselection:"
"`Inställningar --> Anpassa digiKam... --> Diverse --> Utseende`, särskilt "
"med mörka färgscheman. Se :ref:`det här avsnittet <appearance_settings>` i "
"handboken för mer information om alternativen för **Utseende**."

#: ../../setup_application/theme_settings.rst:32
msgid ""
"the table below contains examples of the appearance of the Album-View with "
"each available theme."
msgstr ""
"Tabellen nedan innehåller exempel på albumvisningens utseende med varje "
"tillgängligt tema."

#: ../../setup_application/theme_settings.rst:40
msgid "The Black-Body Theme"
msgstr "Temat Black Body"

#: ../../setup_application/theme_settings.rst:40
msgid "The Color-Contrast Theme"
msgstr "Temat ColorContrast"

#: ../../setup_application/theme_settings.rst:47
msgid "The Dark-Room Theme"
msgstr "Temat DarkRoom"

#: ../../setup_application/theme_settings.rst:47
msgid "The Fusion-Gray Theme"
msgstr "Temat FusionGray"

#: ../../setup_application/theme_settings.rst:54
msgid "The Gray-Card Theme"
msgstr "Temat GrayCard"

#: ../../setup_application/theme_settings.rst:54
msgid "The High-Key Theme"
msgstr "Temat High Key"

#: ../../setup_application/theme_settings.rst:61
msgid "The Low-Key Theme"
msgstr "Temat LowKey"

#: ../../setup_application/theme_settings.rst:61
msgid "The Shade-of-Gray Theme"
msgstr "Temat ShadeOfGray"

#: ../../setup_application/theme_settings.rst:68
msgid "The Sunset-Color Theme"
msgstr "Temat SunsetColor"

#: ../../setup_application/theme_settings.rst:68
msgid "The White-Balance Theme"
msgstr "Temat White Balance"

#~ msgid "Theme Settings"
#~ msgstr "Temainställningar"

#~ msgid "Contents"
#~ msgstr "Innehåll"

#~ msgid "Look below all themed screenshots taken with Album-View."
#~ msgstr "Se nedan för alla skärmbilder med teman tagna med albumvisningen."
