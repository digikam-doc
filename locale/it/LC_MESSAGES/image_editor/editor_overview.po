# Copyright (C) licensed under the  <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">licensed under the terms of the GNU Free Documentation License 1.2+</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
# SPDX-FileCopyrightText: 2023, 2024 Valter Mura <valtermura@gmail.com>
#
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.2.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-12-11 01:44+0000\n"
"PO-Revision-Date: 2024-12-14 23:16+0100\n"
"Last-Translator: Valter Mura <valtermura@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 24.08.3\n"

#: ../../image_editor/editor_overview.rst:1
msgid "Overview to digiKam Image Editor"
msgstr "Panoramica sull'Editor delle immagini di digiKam"

#: ../../image_editor/editor_overview.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, image, editor"
msgstr ""
"digiKam, documentazione, manuale utente, gestione fotografie, open source, "
"libero, apprendimento, facile, editor di immagini"

#: ../../image_editor/editor_overview.rst:14
msgid ":ref:`Overview <image_editor>`"
msgstr ":ref:`Panoramica <image_editor>`"

#: ../../image_editor/editor_overview.rst:16
msgid ""
"digiKam incorporates a fast Image Editor with a few basic yet powerful image "
"editing tools. You can use the Image Editor to view your photographs and to "
"make corrections and alterations. The Image Editor can be accessed either by "
"clicking on a thumbnail in the Image Window or by clicking with the right "
"mouse button over a thumbnail and selecting **Edit** from the context menu."
msgstr ""
"digiKam incorpora un editor delle immagini veloce, che possiede alcuni "
"strumenti di modifica semplici ma avanzati; puoi usarlo per visualizzare le "
"tue foto, oppure per fare delle correzioni o delle modifiche. L'editor delle "
"immagini è accessibile sia facendo clic su una miniatura nella finestra "
"delle immagini, sia con un clic sul pulsante destro del mouse su una "
"miniatura e selezionando **Modifica** dal menu contestuale."

#: ../../image_editor/editor_overview.rst:18
msgid ""
"The Image Editor provides a number of powerful tools that use you to adjust "
"a photograph."
msgstr ""
"L'editor delle immagini ti fornisce degli strumenti avanzati da utilizzare "
"per regolare una fotografia."

#: ../../image_editor/editor_overview.rst:24
msgid "The digiKam Image Editor Main Window"
msgstr "La finestra principale dell'Editor delle immagini di digiKam"

#: ../../image_editor/editor_overview.rst:26
msgid "The image editor has just one main window with:"
msgstr "L'editor delle immagini ha solo una finestra principale con:"

#: ../../image_editor/editor_overview.rst:28
msgid ""
"A status bar at the bottom which shows the filename, the current file "
"number, the current zoom level, and the current image size."
msgstr ""
"Una barra di stato, in basso, che mostra il nome del file, il numero del "
"file attuale, il livello di ingrandimento attuale e le dimensioni "
"dell'immagine attuale."

#: ../../image_editor/editor_overview.rst:30
msgid ""
"A menu bar across the top, and tool bar just below that, for quick access to "
"some commonly used functions."
msgstr ""
"Una barra dei menu, in alto, e una barra degli strumenti appena sotto, per "
"un rapido accesso ad alcune funzioni di uso comune."

#: ../../image_editor/editor_overview.rst:32
msgid ""
"An optional thumb bar (see :menuselection:`Settings --> Show Thumbbar` menu "
"entry) located on the left of canvas. This can be relocated to the top, "
"right, or bottom side by dragging the anchor on the left side of thumb bar "
"to the desired location."
msgstr ""
"Una barra delle miniature opzionale (vedi la voce di menu :menuselection:"
"`Impostazioni --> Mostra barra delle miniature`) posizionata alla sinistra "
"delle tela. Questa può essere riposizionata in alto, a destra o in basso "
"trascinando l'ancora presente sul lato sinistro della barra delle miniature "
"nella posizione desiderata."

#: ../../image_editor/editor_overview.rst:39
msgid "Screencast of Image Editor Thumb Bar Placed at Different Canvas Sides"
msgstr ""
"Registrazione della barra delle miniature dell'Editor delle immagini "
"posizionata in lati diversi della tela"

#: ../../image_editor/editor_overview.rst:41
msgid "From left to right, the Image Editor Toolbar includes these actions:"
msgstr ""
"Da sinistra a destra, la barra degli strumenti dell'Editor delle immagini "
"include le azioni seguenti:"

#: ../../image_editor/editor_overview.rst:43
msgid "Close image editor Window."
msgstr "Chiudi la finestra dell'Editor delle immagini."

#: ../../image_editor/editor_overview.rst:45
msgid "Open original image from the versioning stack."
msgstr "Apri l'immagine originale dalla pila delle versioni."

#: ../../image_editor/editor_overview.rst:47
msgid "Export image to a new file format."
msgstr "Esporta l'immagine in un nuovo formato di file."

#: ../../image_editor/editor_overview.rst:49
msgid "Save changes in versioning stack using default format."
msgstr ""
"Salva le modifiche nella pila delle versioni utilizzando il formato "
"predefinito."

#: ../../image_editor/editor_overview.rst:51
msgid "Save the modified image as a new version using a specific format."
msgstr ""
"Salva l'immagine modificata come una nuova versione utilizzando un formato "
"specificato."

#: ../../image_editor/editor_overview.rst:53
msgid "Cancel all modifications done on the image."
msgstr "Annulla tutte le modifiche apportate all'immagine."

#: ../../image_editor/editor_overview.rst:55
msgid "Undo last action."
msgstr "Annulla l'ultima azione."

#: ../../image_editor/editor_overview.rst:57
msgid "Redo previous action."
msgstr "Rifai l'ultima azione."

#: ../../image_editor/editor_overview.rst:63
msgid "The digiKam Image Editor Toolbar"
msgstr "La barra degli strumenti dell'editor delle immagini di digiKam"

#~ msgid "Overview"
#~ msgstr "Panoramica"

#~ msgid "Contents"
#~ msgstr "Contenuto"
