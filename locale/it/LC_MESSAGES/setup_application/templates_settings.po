# Copyright (C) licensed under the  <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">licensed under the terms of the GNU Free Documentation License 1.2+</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
# SPDX-FileCopyrightText: 2023, 2024, 2025 Valter Mura <valtermura@gmail.com>
#
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-12-15 02:27+0000\n"
"PO-Revision-Date: 2025-01-05 21:00+0100\n"
"Last-Translator: Valter Mura <valtermura@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 24.12.0\n"

#: ../../setup_application/templates_settings.rst:1
msgid "digiKam Templates Settings"
msgstr "Impostazioni dei modelli di digiKam"

#: ../../setup_application/templates_settings.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, configure, setup, template, metadata, copyright, author, title, "
"credit, source, IPTC, XMP"
msgstr ""
"digiKam, documentazione, manuale utente, gestione fotografie, open source, "
"libero, apprendimento, facile, configurare, configurazione, modello, "
"copyright, autore, titolo, riconoscimento, sorgente, IPTC, XMP"

#: ../../setup_application/templates_settings.rst:14
msgid ":ref:`Templates Settings <setup_application>`"
msgstr ":ref:`Impostazioni dei modelli <setup_application>`"

#: ../../setup_application/templates_settings.rst:16
msgid "Contents"
msgstr "Indice"

#: ../../setup_application/templates_settings.rst:18
msgid ""
"The **Templates** options for digiKam are set from :menuselection:`Settings "
"--> Configure digiKam... --> Templates page`."
msgstr ""
"Le opzioni dei **Modelli** di digiKam vengono configurate da :menuselection:"
"`Impostazioni --> Configura digiKam... --> pagina Modelli`."

#: ../../setup_application/templates_settings.rst:21
msgid "Overview"
msgstr "Panoramica"

#: ../../setup_application/templates_settings.rst:23
msgid ""
"The **Metadata Templates Manager** is meant to create and manage metadata "
"templates which can be used in:"
msgstr ""
"Il **gestore dei modelli di metadati** è pensato per creare e gestire i "
"modelli di metadati, che possono essere usati:"

#: ../../setup_application/templates_settings.rst:25
msgid ""
"The **Information** tab of the **Captions** view from the Right Sidebar."
msgstr ""
"nella scheda **Informazioni** della vista **Didascalie** nella barra "
"laterale destra."

#: ../../setup_application/templates_settings.rst:27
msgid ""
"The **On the Fly Operations (JPEG only)** section of the **Settings** view "
"from the Right Sidebar in the :menuselection:`Import --> Cameras` Window."
msgstr ""
"Nella sezione **Operazioni al volo (solo JPEG)** della vista "
"**Impostazioni** nella barra laterale destra nella finestra :menuselection:"
"`Importa --> Fotocamere`."

#: ../../setup_application/templates_settings.rst:29
msgid ""
"The goal of templates is to apply multiple pieces of metadata to one or more "
"items at the same time. You can create many different profiles corresponding "
"to different aspects of your workflow. In all cases, the information will be "
"stored in the file as either XMP or IPTC metadata. Remember that XMP is a "
"replacement of IPTC as explained :ref:`in this section <iptc_tags>` of the "
"manual."
msgstr ""
"Lo scopo dei modelli è di applicare più parti dei metadati a uno o più "
"elemento alla volta. Puoi creare molti profili diversi che corrispondono ai "
"diversi aspetti del tuo flusso di lavoro. In ogni caso, le informazioni "
"saranno memorizzate nel file come metadati XMP o IPTC. Ricorda che XMP è una "
"sostituzione di IPTC, come spiegato :ref:`in questo capitolo <iptc_tags>` "
"del manuale."

#: ../../setup_application/templates_settings.rst:31
msgid ""
"The top portion of the templates view shows a list of the already existing "
"templates. The first column shows the title of the template, and the second "
"column shows the name of the author(s) in the “Author Names” field of that "
"template. To the right you have three editing buttons, below an input field "
"for the template title. To create a new template you type a title in the "
"input field and click the **Add** button. The new template will appear in "
"the list. To edit an existing template you select it in the list, edit its "
"fields (see next paragraph) and then click the **Replace** button. You can "
"also type in a new title for the edited template and save the new version "
"with the **Add** button. To delete a template, very obviously, you select it "
"in the list and click the **Remove** button."
msgstr ""
"La porzione superiore della vista dei modelli mostra un elenco dei modelli "
"già esistenti. La prima colonna mostra il titolo del modello, la seconda il "
"nome degli autori, nel campo *Nomi degli autori*: di quel modello. A destra "
"ci sono tre pulsanti di modifica, sotto un campo di immissione per il titolo "
"del modello. Per creare un nuovo modello scrivi il titolo nel campo di "
"immissione, e fai clic sul pulsante **Aggiungi**: il nuovo modello apparirà "
"nell'elenco. Per modificare un modello esistente selezionalo nell'elenco, "
"modifica i suoi campi (vedi il paragrafo successivo), quindi fai clic sul "
"pulsante **Sostituisci**. Puoi anche inserire un nuovo titolo per il modello "
"modificato, e salvare la nuova versione con il pulsante **Aggiungi**. Per "
"eliminare un modello, selezionalo nell'elenco e fai clic sul pulsante "
"**Rimuovi**."

#: ../../setup_application/templates_settings.rst:35
msgid ""
"Note that all the changes you make to the templates will only be stored in "
"the template file if you exit the settings dialog with **OK** button. The "
"**Add** and **Replace** buttons only change the template list stored in "
"memory."
msgstr ""
"Nota che tutti le modifiche che apporti ai modelli saranno memorizzate nel "
"file del modello se chiudi la finestra delle impostazioni con il pulsante "
"**OK**. I pulsanti **Aggiungi** e **Rimuovi** cambiano solamente l'elenco "
"dei modelli salvato in memoria."

#: ../../setup_application/templates_settings.rst:39
msgid ""
"The template file can be found in :file:`~/.local/share/digikam/` for manual "
"backup or for managing different template sets by means of the file system."
msgstr ""
"Il file del modello può essere trovato in :file:`~/.local/share/digikam/` "
"per la copia di sicurezza manuale o per gestire diversi insiemi di modelli "
"per mezzo del filesystem."

#: ../../setup_application/templates_settings.rst:41
msgid ""
"The rest of the window is divided into four tabs, grouping the metadata "
"fields into **Rights**, **Location**, **Contact** and **Subjects**."
msgstr ""
"Il resto della finestra è diviso in quattro schede, che raggruppano i campi "
"dei metadati in **Diritti**, **Posizione**, **Contatto** e **Soggetti**."

#: ../../setup_application/templates_settings.rst:44
msgid "Rights Information"
msgstr "Informazioni sui diritti"

#: ../../setup_application/templates_settings.rst:50
msgid "The digiKam Template Rights Configuration Page"
msgstr "La pagina di configurazione Diritti del modello di digiKam"

#: ../../setup_application/templates_settings.rst:52
msgid ""
"The **Rights** tab contains the default identity and copyright data. This is "
"an extract of the IPTC/XMP specifications for these fields:"
msgstr ""
"La scheda **Diritti** contiene l'identità predefinita e i dati sul "
"copyright. Questo è un estratto delle specifiche IPTC/XMP per questi campi:"

#: ../../setup_application/templates_settings.rst:54
msgid ""
"**Authors** (synonymous to *Creator and By-line*): This field should contain "
"your name, or the name of the person who created the photograph. If it is "
"not appropriate to add the name of the photographer (for example, if the "
"identity of the photographer needs to be protected) the name of a company or "
"organization can also be used. Once saved, this field should not be changed "
"by anyone. This field does support the use of semi-colons as a separator. "
"With IPTC, this field is limited to 32 characters."
msgstr ""
"**Autori** (sinonimo di *Creatore e Intestazione*): questo campo deve "
"contenere il tuo nome o quello della persona che ha creato la fotografia. Se "
"non è corretto aggiungere il nome del fotografo (per esempio, se l'identità "
"del fotografo deve essere protetta), può essere anche utilizzato il nome di "
"un'azienda o di un'organizzazione. Una volta salvato, questo campo non "
"dovrebbe essere modificato da nessuno. Il campo accetta l'uso di punti e "
"virgola come separatori. Con IPTC, questo campo è limitato a 32 caratteri."

#: ../../setup_application/templates_settings.rst:56
msgid ""
"**Author Positions** (synonymous with *By-line title*): This field should "
"contain the job title of the photographer. Examples might include titles "
"such as: Staff Photographer, Freelance Photographer, or Independent "
"Commercial Photographer. Since this is a qualifier for the Author field, the "
"Author field must also be filled out. With IPTC, this field is limited to 32 "
"characters."
msgstr ""
"**Posizioni degli autori** (sinonimo di *Titolo dell'intestazione*): questo "
"campo deve contenere il titolo di lavoro del fotografo. Esempi potrebbero "
"includere titoli quali: fotografo dello staff, fotografo freelance o "
"commerciale indipendente. Dato che è un qualificatore del campo Autore, "
"anche quest'ultimo deve essere compilato. Con IPTC, questo campo è limitato "
"a 32 caratteri."

#: ../../setup_application/templates_settings.rst:58
msgid ""
"**Credit** (synonymous with *Provider*): Use the credit field to identify "
"who is providing the photograph. This does not necessarily have to be the "
"author. If a photographer is working for a news agency such as Reuters or "
"the Associated Press, these organizations could be listed here as they are "
"\"providing\" the image for use by others. If the image is a stock "
"photograph, then the group (agency) involved in supplying the image should "
"be listed here. With IPTC, this field is limited to 32 characters."
msgstr ""
"**Riconoscimenti** (sinonimo di *Fornitore*): usa il campo Riconoscimenti "
"per identificare chi fornisce la fotografia. Non dev'essere necessariamente "
"l'autore. Se un fotografo lavora per un'agenzia di stampa, come la Reuters o "
"la Associated Press, questa potrebbe essere elencata come «fornitore» "
"dell'immagine agli utenti. Se l'immagine è una fotografia di repertorio, il "
"gruppo (agenzia) che la fornisce dovrebbe essere indicato qui. Con IPTC, "
"questo campo è limitato a 32 caratteri."

#: ../../setup_application/templates_settings.rst:60
msgid ""
"**Copyright**: The Copyright Notice should contain any necessary copyright "
"notice for claiming the intellectual property, and should identify the "
"current owner(s) of the copyright for the photograph. Usually, this would be "
"the photographer, but if the image was done by an employee or as work-for-"
"hire, then the agency or company should be listed. Use the form appropriate "
"to your country. For the United States you would typically follow the form "
"of (c) {date of first publication} name of copyright owner, as in *(c) 2005 "
"John Doe.* Note, the word *copyright* or the abbreviation *copr* may be used "
"in place of the (c) symbol. In some foreign countries only the copyright "
"symbol is recognized and the abbreviation does not work. Furthermore the "
"copyright symbol must be a full circle with a *c* inside; using something "
"like (c) where the parentheses form a partial circle is not sufficient. For "
"additional protection worldwide, use of the phrase, \"all rights reserved\" "
"following the notice above is encouraged. In Europe you would use: Copyright "
"{Year} {Copyright owner}, all rights reserved. In Japan, for maximum "
"protection, the following three items should appear in the copyright field "
"of the IPTC Core: (a) the word, Copyright; (b) year of the first "
"publication; and (c) name of the author. You may also wish to include the "
"phrase *all rights reserved*."
msgstr ""
"**Diritto d'autore**: la nota del diritto d'autore deve contenere tutti i "
"dettagli legali per far valere la proprietà intellettuale, e deve "
"identificare l'attuale proprietario (o proprietari) del diritto d'autore "
"della fotografia. Di solito questo è il fotografo, ma se l'immagine è stata "
"creata da un'impiegato o un esterno a pagamento, allora deve essere elencato "
"l'agenzia o l'azienda. Usa la forma idonea al tuo paese. Per gli Stati Uniti "
"la forma segue in genere (c) {data della prima pubblicazione} none del "
"proprietario del diritto d'autore, come in *(c) 2005 John Doe.* Nota che può "
"essere utilizzata la parola *copyright* o la sua abbreviazione *copr* al "
"posto del simbolo (c). In alcuni paesi stranieri viene riconosciuto solo il "
"simbolo del copyright e l'abbreviazione non vale. Inoltre, il simbolo del "
"copyright deve essere un cerchio intero con una *c* al suo interno; non è "
"sufficiente qualcosa di simile a una (c) in cui le parentesi formano un "
"cerchio parziale. Per protezione aggiuntiva a livello mondiale, è "
"consigliato l'uso della frase «tutti i diritti riservati»\" di seguito alla "
"notifica. In Europa, utilizzare: Copyright {Anno} {proprietario del diritto "
"d'autore}, tutti i diritti riservati. In Giappone, per la massima "
"protezione, devono apparire i seguenti tre elementi nel campo del diritto "
"d'autore del IPTC Core: (a) la parola, Copyright; (b) anno della prima "
"pubblicazione; e (c) nome dell'autore. Potresti anche voler includere la "
"frase *tutti i diritti riservati*."

#: ../../setup_application/templates_settings.rst:62
msgid ""
"The copyright marking for a Creative Commons Share-alike license might read:"
msgstr ""
"Il contrassegno del copyright per una licenza Creative Commons Share-alike "
"potrebbe recitare:"

#: ../../setup_application/templates_settings.rst:64
msgid "*This work copyright {Year} by {Author} is licensed under CC BY-SA 4.0*"
msgstr ""
"*Quest'opera è copyright {Anno} di {Autore} ed è sotto licenza CC BY-SA 4.0*"

#: ../../setup_application/templates_settings.rst:66
msgid ""
"See `<https://creativecommons.org/licenses/by-sa/4.0/>`_ for details on what "
"the Share-Alike license means."
msgstr ""
"Vedi `<https://creativecommons.org/licenses/by-sa/4.0/>`_ per i dettagli su "
"cosa significa la licenza Share-Alike."

#: ../../setup_application/templates_settings.rst:68
msgid ""
"With XMP, you can include more than one copyright string using different "
"languages. With IPTC, this field is limited to 128 characters."
msgstr ""
"Con XMP, puoi includere più di una stringa di diritto d'autore, in lingue "
"differenti. Con IPTC, questo campo è limitato a 128 caratteri."

#: ../../setup_application/templates_settings.rst:70
msgid ""
"**Right Usage Terms**: The Right Usage Terms field should be used to list "
"instructions on how a resource can be legally used. With XMP, you can "
"include more than one right usage terms string using different languages. "
"This field does not exist with IPTC."
msgstr ""
"**Termini di utilizzo**: questo campo dovrebbe essere usato per elencare le "
"istruzioni su come una risorsa possa essere usata legalmente. Con XMP puoi "
"includere più di una stringa di termini di utilizzo, in lingue differenti. "
"Questo campo non esiste in IPTC."

#: ../../setup_application/templates_settings.rst:72
msgid ""
"**Source**: The Source field should be used to identify the original owner "
"or copyright holder of the photograph. The value of this field should never "
"be changed after the information is entered following the image's creation. "
"While not yet enforced by the custom panels, you should consider this to be "
"a \"write-once\" field. The source could be an individual, an agency, or a "
"member of an agency. To aid in later searches, it is suggested to separate "
"any slashes */* with a blank space. Use the form *photographer / agency* "
"rather than *photographer/agency*. Source may also be different from Creator "
"and from the names listed in the **Copyright Notice**. With IPTC, this field "
"is limited to 32 characters."
msgstr ""
"**Fonte**: questo campo dovrebbe essere usato per identificare il "
"proprietario originale o il possessore del diritto d'autore della "
"fotografia. Il valore di questo campo non deve mai essere cambiato dopo che "
"l'informazione è stata inserita, a seguito della creazione dell'immagine. "
"Sebbene non sia ancora imposto dai pannelli personalizzati, dovresti "
"considerare questo campo come a «scrittura singola». La fonte può essere una "
"persona, un'agenzia, o il membro di un'agenzia. Per facilitare le ricerche "
"successive, suggeriamo di separare le barre */* con uno spazio. Usa la forma "
"*fotografo / agenzia* anziché *fotografo/agenzia*. La fonte potrebbe inoltre "
"essere diversa dal Creatore e dai nomi elencati nella Nota del diritto "
"d'autore. Con IPTC, questo campo è limitato a 32 caratteri."

#: ../../setup_application/templates_settings.rst:74
msgid ""
"**Instructions**: The Instructions field should be used to list editorial "
"instructions concerning the use of photograph. With IPTC, this field is "
"limited to 256 characters."
msgstr ""
"**Istruzioni**:  questo campo dovrebbe essere usato per elencare istruzioni "
"editoriali che riguardino l'uso della fotografia. Con IPTC, questo campo è "
"limitato a 256 caratteri."

#: ../../setup_application/templates_settings.rst:77
msgid "Location Information"
msgstr "Informazioni sulla posizione"

#: ../../setup_application/templates_settings.rst:83
msgid "The digiKam Template Location Configuration Page"
msgstr "La pagina di configurazione Posizioni del modello di digiKam"

#: ../../setup_application/templates_settings.rst:85
msgid ""
"The **Location** tab contains the default data about the place in the world "
"describing the contents. This is an extract of the IPTC/XMP specifications "
"for these fields:"
msgstr ""
"La scheda **Posizione** contiene i dati predefiniti sul luogo nel mondo che "
"descrive il contenuto. Questo è un estratto delle specifiche IPTC/XMP per "
"questi campi:"

#: ../../setup_application/templates_settings.rst:87
msgid ""
"**City**: This field should contain the name of the city where the "
"photograph was taken. With IPTC, this field is limited to 32 characters."
msgstr ""
"**Città**: questo campo deve contenere il nome della città in cui è stata "
"scattata la fotografia. Con IPTC, questo campo è limitato a 32 caratteri."

#: ../../setup_application/templates_settings.rst:89
msgid ""
"**Sublocation**: This field should contain the sublocation of the city where "
"the photograph was taken. With IPTC, this field is limited to 32 characters."
msgstr ""
"**Posto**: questo campo deve contenere il nome del posto in cui è stata "
"scattata la fotografia. Con IPTC, questo campo è limitato a 32 caratteri."

#: ../../setup_application/templates_settings.rst:91
msgid ""
"**State/Province**: This field should contain the province or state where "
"the photograph was taken. With IPTC, this field is limited to 32 characters."
msgstr ""
"**Regione o provincia**: questo campo deve contenere il nome della regione o "
"provincia in cui è stata scattata la fotografia. Con IPTC, questo campo è "
"limitato a 32 caratteri."

#: ../../setup_application/templates_settings.rst:93
msgid ""
"**Country**: This field should contain the country where the photograph was "
"taken. With IPTC, this field is limited to 64 characters."
msgstr ""
"**Nazione**: questo campo deve contenere la nazione in cui è stata scattata "
"la fotografia. Con IPTC, questo campo è limitato a 64 caratteri."

#: ../../setup_application/templates_settings.rst:96
msgid "Contact Information"
msgstr "Informazioni sul contatto"

#: ../../setup_application/templates_settings.rst:102
msgid "The digiKam Template Contact Configuration Page"
msgstr "La pagina di configurazione Contatto del modello di digiKam"

#: ../../setup_application/templates_settings.rst:104
msgid ""
"The data in the Contact tab refers to the (lead) photographer under **Author "
"Names** in the **Rights** tab. Since these fields do not exist in IPTC, an "
"extract of the XMP specifications for these fields is listed below:"
msgstr ""
"I dati nella scheda Contatto si riferiscono al fotografo (principale) che si "
"trova in **Nomi degli autori** nella scheda **Diritti**. Poiché questi campi "
"non esistono in IPTC, sotto viene elencato un estratto delle specifiche XMP "
"per questi campi:"

#: ../../setup_application/templates_settings.rst:106
msgid ""
"**Address**: This field should contain the address where the lead author "
"lives."
msgstr ""
"**Indirizzo**: questo campo dovrebbe contenere l'indirizzo dove l'autore "
"principale vive."

#: ../../setup_application/templates_settings.rst:108
msgid ""
"**Postal Code**: This field should contain the postal code where the lead "
"author lives."
msgstr ""
"**Codice postale**: questo campo dovrebbe contenere il codice postale dove "
"l'autore principale vive."

#: ../../setup_application/templates_settings.rst:110
msgid ""
"**City**: This field should contain the city name where the lead author "
"lives."
msgstr ""
"**Città**: questo campo dovrebbe contenere il nome della città dove l'autore "
"vive."

#: ../../setup_application/templates_settings.rst:112
msgid ""
"**Province**: This field should contain the province where the lead author "
"lives."
msgstr ""
"**Provincia**: questo campo dovrebbe contenere la provincia dove l'autore "
"principale vive."

#: ../../setup_application/templates_settings.rst:114
msgid ""
"**Country**: This field should contain the country name where the lead "
"author lives."
msgstr ""
"**Nazione**: questo campo dovrebbe contenere il nome di nazione dove "
"l'autore principale vive."

#: ../../setup_application/templates_settings.rst:116
msgid ""
"**Phone**: This field should contain the phone number of the lead author."
msgstr ""
"**Telefono**: questo campo dovrebbe contenere il numero di telefono "
"dell'autore principale."

#: ../../setup_application/templates_settings.rst:118
msgid "**Email**: This field should contain the email of the lead author."
msgstr ""
"**Posta elettronica**: questo campo dovrebbe contenere l'indirizzo di posta "
"elettronica dell'autore principale."

#: ../../setup_application/templates_settings.rst:120
msgid "**URL**: This field should contain the web site URL of the lead author."
msgstr ""
"**URL**: questo campo dovrebbe contenere l'indirizzo del sito internet "
"dell'autore principale."

#: ../../setup_application/templates_settings.rst:123
msgid "Subjects Information"
msgstr "Informazioni sui soggetti"

#: ../../setup_application/templates_settings.rst:129
msgid "The digiKam Template Subjects Configuration Page"
msgstr "La pagina di configurazione Soggetti del modello di digiKam"

#: ../../setup_application/templates_settings.rst:131
msgid ""
"In the **Subjects** tab you can assign one or more Subject Codes according "
"to the IPTC Photo Metadata Standard to the template. If the first option "
"**Use standard reference code** is selected you can choose a code from the "
"drop-down field and the meaning of it will then appear in the fields of the "
"**Use custom definition** section. If you don't know the code for the "
"subject you want to assign this way is a bit tedious since there are 1400 "
"subjects in the standard. A better way is to look for your subject in `this "
"URL <https://show.newscodes.org/index.html?newscodes=subj&lang=en-"
"GB&startTo=Show>`_. There you select **Subject Codes** and your language "
"(English recommended) and click *Show*. You can scroll through the diagram "
"and make additional levels visible by clicking on one of the fields. In the "
"field right of the diagram you can find the code of the selected subject."
msgstr ""
"Nella scheda **Soggetti** puoi assegnare uno o più Codici di soggetto, in "
"base allo standard dei metadati IPTC del modello. Se è selezionata la prima "
"opzione, **Usa un codice di riferimento standard**, puoi scegliere un codice "
"da un campo a tendina, e il suo significato apparirà quindi nel campo della "
"sezione **Usa definizione personalizzata**. Questa modalità è un po' noiosa "
"se non conosci il codice per il soggetto a cui lo vuoi assegnare, dato che "
"ci sono 1400 soggetti nello standard. Un sistema migliore è quello di "
"cercare il tuo soggetto in `quest'URL <https://show.newscodes.org/index.html?"
"newscodes=subj&lang=en-GB&startTo=Show>`_. Qui selezioni i **Codici di "
"soggetto** e la tua lingua (è raccomandato l'inglese), poi fai clic su "
"**Mostra**. Puoi scorrere attraverso il diagramma ed entrare negli ulteriori "
"livelli facendo clic su uno dei campi. Nel campo alla destra del diagramma è "
"possibile trovare il codice del soggetto selezionato."

#: ../../setup_application/templates_settings.rst:133
msgid ""
"In general the IPTC Subject Codes are quite comprehensive but on the other "
"hand a bit incomplete in some fields, e.g. under Lifestyle and Leisure/Games "
"you find just Go, Chess, Bridge and Shogi. So you may want to add your own "
"subjects, even though they always remain private (or company) subjects. A "
"way to do that in digiKam is to first check **Use standard reference code** "
"and select *10001004* which brings you to *Lifestyle* and *Leisure/Games/"
"shogi* - just to continue with our example. Then you check **Use custom "
"definition** and change the last digit of **Reference** to 5 and the text in "
"the **Detail** field to - say *domino*. You save this custom definition to "
"the template by clicking **Add** button at the right side of the subjects "
"list. Then you type in a template title (if it's a new template) and save "
"the template by clicking the **Add** button at the right side of the "
"templates list. Don't mix up these two."
msgstr ""
"In generale, i Codici di soggetto IPTC sono abbastanza esaurienti, ma "
"d'altro lato un po' incompleti in alcuni campi: per esempio, sotto Tempo "
"libero/Giochi trovi solo Go, Scacchi, Bridge e Shogi. Potresti dunque voler "
"aggiungere dei soggetti personalizzati, sebbene restino sempre soggetti "
"privati (o aziendali). Un modo per farlo in digiKam è spuntare prima **Usa "
"un codice di riferimento standard**, quindi scegliere *10001004*, che ti "
"porta a *Tempo libero/Giochi/shogi* (tanto per continuare col nostro "
"esempio). Spunta quindi **Usa definizione personalizzata**, cambia l'ultima "
"cifra di **Riferimento** in 5 e il testo nel campo **Dettagli** con, per "
"esempio, *domino*. Salva questa definizione personalizzata nel modello, "
"facendo clic sul pulsante **Aggiungi**, a destra dell'elenco dei soggetti. "
"Scrivi quindi un titolo per il modello (se è nuovo) e salvalo facendo clic "
"sul pulsante **Aggiungi**, a destra dell'elenco dei modelli. Non mischiare "
"questi due."

#~ msgid "Templates Settings"
#~ msgstr "Impostazioni dei modelli"

#~ msgid ""
#~ "**Country**: Select here the country where the photograph was taken. With "
#~ "IPTC, this field is limited to 64 characters."
#~ msgstr ""
#~ "**Nazione**: seleziona qui la nazione in cui è stata scattata la "
#~ "fotografia. Con IPTC, questo campo è limitato a 64 caratteri."
