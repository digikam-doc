# Copyright (C) licensed under the  <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">licensed under the terms of the GNU Free Documentation License 1.2+</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
# SPDX-FileCopyrightText: 2023, 2024 Valter Mura <valtermura@gmail.com>
#
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-12-08 01:42+0000\n"
"PO-Revision-Date: 2024-12-10 19:28+0100\n"
"Last-Translator: Valter Mura <valtermura@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 24.08.3\n"

#: ../../batch_queue/batchqueue_overview.rst:1
msgid "Overview to digiKam Batch Queue Manager"
msgstr "Panoramica del Gestore elaborazione in serie di digiKam"

#: ../../batch_queue/batchqueue_overview.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, batch, queue"
msgstr ""
"digiKam, documentazione, manuale utente, gestione fotografie, open source, "
"libero, apprendimento, facile, in serie, coda"

#: ../../batch_queue/batchqueue_overview.rst:14
msgid ":ref:`Overview <batch_queue>`"
msgstr ":ref:`Panoramica <batch_queue>`"

#: ../../batch_queue/batchqueue_overview.rst:16
msgid ""
"digiKam features a batch queue manager that opens in a separate window to "
"easily batch process a list of items, aka filtering, converting, "
"transforming, etc. Batch processing works with all supported image formats "
"including RAW files."
msgstr ""
"digiKam presenta un gestore elaborazione in serie che si apre in finestra "
"separata, per elaborare in modo semplice un elenco di elementi in serie, per "
"esempio filtraggio, conversione, trasformazione, ecc. L'elaborazione in "
"serie funziona con tutti i formati immagine supportati, incluso i file RAW."

#: ../../batch_queue/batchqueue_overview.rst:18
msgid ""
"To select images for batch processing, select one or several images in any "
"view from the main window, then select **Add to Current Queue** :kbd:`Ctrl"
"+B` from the context menu. The selection will be added to the batch queue, "
"and a separate Batch Queue Manager window will open. You can also quickly "
"open the Batch Queue Manager window from the digiKam **Main Window** by "
"selecting :menuselection:`Tools --> Batch Queue Manager` :kbd:`B`, or by "
"clicking **Batch Queue Manager** in the main toolbar."
msgstr ""
"Per selezionare le immagini per l'elaborazione in serie, seleziona una o più "
"immagini in una vista qualsiasi della finestra principale, quindi scegli "
"**Aggiungi alla coda corrente** :kbd:`Ctrl+B` dal menu contestuale. La "
"selezione verrà aggiunta all'elaborazione in serie, che aprirà una finestra "
"separata del Gestore elaborazione in serie. Puoi anche aprire rapidamente la "
"finestra del Gestore elaborazione in serie dalla **finestra principale** di "
"digiKam selezionando dal menu :menuselection:`Strumenti --> Gestore "
"elaborazione in serie` :kbd:`B`, oppure facendo clic sul **Gestore "
"elaborazione in serie** nella barra degli strumenti principale."

#: ../../batch_queue/batchqueue_overview.rst:24
msgid "The Batch Queue Manager Window"
msgstr "La finestra del Gestore di elaborazione in serie"

#: ../../batch_queue/batchqueue_overview.rst:26
msgid ""
"The Batch Queue Manager window displays the list of **Queues** to process on "
"the top left. Queues are a stack of items to batch process, applying the "
"same tools with the same settings to each image in the queue. Each queue is "
"processed sequentially, but queue contents can be processed in parallel, "
"depending on the Queue Settings. The Queue list shows queues as tabs, with "
"each tab listing the items in that queue as an ordered table displaying the "
"item thumbnail, the original filename, and the target filename indicating "
"where the result should be stored."
msgstr ""
"La finestra del Gestore elaborazione in serie visualizza, in alto a "
"sinistra, l'elenco delle **code** da elaborare. Queste sono pile di elementi "
"da elaborare in serie, applicando gli stessi strumenti con le stesse "
"impostazioni a ciascuna immagine della coda. Ogni coda viene elaborata in "
"sequenza, ma il suo contenuto può essere elaborato in parallelo, a seconda "
"delle impostazioni della coda. L'elenco della coda mostra le code sotto "
"forma di schede, e ciascuna scheda elenca gli elementi appartenenti a quella "
"coda sotto forma di tabella, che mostra la miniatura dell'elemento, il nome "
"file originale e il nome file di destinazione indicante la posizione verrà "
"memorizzato il risultato."

#: ../../batch_queue/batchqueue_overview.rst:30
msgid ""
"If you want to process the items from an album and all sub-albums in a "
"queue, just turn on the option to display sub-albums in the Album-View using "
"the :menuselection:`View --> Include Album-Sub Tree` menu entry, select the "
"corresponding items, and then add them to the Batch Queue Manager."
msgstr ""
"Se desideri elaborare gli elementi da un album e da tutti i suoi album "
"secondari in una coda, attiva l'opzione per visualizzare gli album secondari "
"nella vista Album utilizzando la voce di menu :menuselection:`Vista --> "
"Includi sotto-albero dell'album`, seleziona gli elementi corrispondenti e "
"quindi aggiungili al Gestore elaborazione in serie."

#: ../../batch_queue/batchqueue_overview.rst:32
msgid ""
"The list of **Assigned Tools** to apply to each item in the queue is shown "
"in the top middle of the window. Each tool in this list is applied "
"sequentially from top to bottom. You can drag tools within this list to "
"rearrange the order as needed. It's highly recommended that you export to a "
"new format at the end of this list in order to preserve the best image "
"quality."
msgstr ""
"L'elenco degli strumenti assegnati da applicare a ciascun elemento nella "
"coda è mostrato nella parte superiore al centro della finestra. Ogni "
"strumento nell'elenco viene applicato in sequenza, dall'alto verso il basso. "
"Puoi riordinare gli strumenti all'interno dell'elenco trascinandoli, "
"nell'ordine che preferisci. Ti raccomandiamo caldamente di esportare in un "
"nuovo formato alla fine di questo elenco, in modo da preservare la migliore "
"qualità di immagine."

#: ../../batch_queue/batchqueue_overview.rst:34
msgid ""
"To adjust the settings for a tool, select the tool from the Assigned Tools "
"list. The settings for that tool are then displayed in the **Tool Settings** "
"area in the top right of the window. Any changes to these settings will be "
"saved in the queue and applied to all items during batch processing."
msgstr ""
"Per regolare le impostazioni di uno strumento, selezionalo dall'elenco degli "
"Strumenti assegnati. Le impostazioni per quello strumento vengono quindi "
"visualizzate nell'area **Impostazioni dello strumento** nella parte "
"superiore destra della finestra. Qualsiasi modifica a queste impostazioni "
"verranno salvate nelle coda e applicate a tutti gli elementi durante "
"l'elaborazione in serie."

#: ../../batch_queue/batchqueue_overview.rst:36
msgid ""
"The **Queue Settings** in the bottom left of the window shows all of the "
"root settings for the selected queue. These include where to save the target "
"files, file renaming rules, general behaviors for file handling, settings "
"for processing RAW files, and image settings for the target files. The file "
"renaming view is mostly the same as for the **Advanced Rename** tool "
"available in **Main Window**. See the full description in :ref:`this section "
"<renaming_photograph>` of this manual."
msgstr ""
"Le **Impostazioni della coda**, nella parte inferiore sinistra della "
"finestra, mostrano tutte le impostazioni radice per la coda selezionata. "
"Queste includono la posizione di salvataggio dei file di destinazione, le "
"regole di rinomina dei file, i comportamenti generali per la gestione dei "
"file, le impostazioni per elaborare i file RAW, e le impostazioni delle "
"immagini per i file di destinazione. La vista di rinomina dei file è "
"praticamente la stessa dello strumento di **rinomina avanzata** disponibile "
"nella **finestra principale**. Leggi la descrizione completa in :ref:`questa "
"sezione <renaming_photograph>` del manuale."

#: ../../batch_queue/batchqueue_overview.rst:38
msgid ""
"The **Control Panel** in the bottom right of the window shows the collection "
"of tools available in the batch queue manager. You can assign a tool to the "
"current queue by a double clicking or a drag and drop. This view also shows "
"a list of pre-recorded queue settings that allows you to later replay a "
"previous workflow. Finally the History tab shows a log-file of previous "
"batch operations."
msgstr ""
"Il **pannello di controllo**. a destra in basso della finestra, mostra la "
"raccolta di strumenti disponibili nel Gestore elaborazione in serie. Puoi "
"assegnare uno strumento alla coda corrente facendovi sopra doppio clic "
"oppure trascinandolo e rilasciandolo. Questa vista mostra anche un elenco di "
"impostazioni preregistrate di code in modo che ti permettono di replicare un "
"flusso di lavoro in un secondo momento. Infine, la scheda della Cronologia "
"mostra un file di registro delle operazioni elaborate in serie precedenti."

#: ../../batch_queue/batchqueue_overview.rst:45
msgid "Screencast of Batch Queue Manager Tools Assignment to a Queue"
msgstr ""
"Registrazione di assegnazione di strumenti del Gestore elaborazione in serie "
"a una coda"

#~ msgid "Overview"
#~ msgstr "Panoramica"

#~ msgid "Contents"
#~ msgstr "Contenuto"
